To host your CSS and JavaScript files on GitLab and use them in your Mautic landing page, follow these steps:

### 1. Create a GitLab Repository:

- Log in to your GitLab account.
- Create a new repository where you will host your assets (CSS, JS, images, etc.).

### 2. Upload Your Assets:

- Clone the repository to your local machine or use the GitLab web interface to upload your assets (CSS files, JavaScript files, and any images or other assets used by your landing page).
- Organize your assets in folders as needed (e.g., `css/`, `js/`, `images/`).

### 3. Set Repository to Public:

- Make sure your repository is set to public, so the assets can be accessed by the landing page without authentication.
- Go to your repository settings and set the visibility to "Public".

### 4. Get the URLs of Your Assets:

- Navigate to each asset in your GitLab repository and get its URL. The URL will typically look like:
  ```
  https://gitlab.com/username/repository-name/raw/branch-name/path/to/asset.css
  ```
- Replace `username`, `repository-name`, `branch-name`, and `path/to/asset.css` with your actual username, repository name, branch name, and asset path.

https://gitlab.com/public-assets1/web-assets/raw/main/js/jquery.js?

### 5. Update Paths in Your HTML:

- In your HTML code, replace the paths of your assets with the URLs you obtained from GitLab. For example:

  ```html
  <!-- Before -->
  <link href="assets/css/style.css" rel="stylesheet">
  <script src="assets/js/script.js"></script>

  <!-- After -->
  <link href="https://gitlab.com/username/repository-name/raw/branch-name/css/style.css" rel="stylesheet">
  <script src="https://gitlab.com/username/repository-name/raw/branch-name/js/script.js"></script>
  ```

### 6. Save and Test:

- Save your HTML code with the updated asset paths.
- Test your landing page to ensure all assets are loading correctly from GitLab.

### 7. Publish:

- Once you've confirmed everything is working, publish your landing page in Mautic.

By hosting your assets on GitLab, you can easily manage and update them without having to modify your landing page in Mautic each time. Just make sure to keep the URLs updated if you move or rename your assets in the GitLab repository.